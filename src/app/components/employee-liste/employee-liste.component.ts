import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';

@Component({
  selector: 'app-employee-liste',
  templateUrl: './employee-liste.component.html',
  styleUrls: ['./employee-liste.component.scss']
})
export class EmployeeListeComponent implements OnInit {

  Employee: any = [];

  constructor(private apiService: ApiService) {
    this.readEmployee();
  }

  ngOnInit(): void {
  }

  readEmployee() {
    this.apiService.getEmployees().subscribe((data) => {
      this.Employee = data;
    });

  }

  removeEmployee(employee, index) {
    if (window.confirm('Are you sure ?')) {
      this.apiService.deleteEmployee(employee._id).subscribe((data) => {
        this.Employee.splice(index, 1);
      });
    }
  }

}
